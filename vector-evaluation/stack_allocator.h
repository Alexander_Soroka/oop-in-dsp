#ifndef STACK_ALLOCATOR_H
#define STACK_ALLOCATOR_H

#include <cstddef>

extern char* stack_memory_head;

void InitializeStackAllocator(size_t size);

template<class Type>
class StackAllocator
{
  char* head_;

public:
  typedef Type value_type;
  typedef Type* pointer;
  typedef Type& reference;
  typedef const Type* const_pointer;
  typedef const Type& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  StackAllocator()
      : head_(stack_memory_head)
  {
  }

  template <class T>
  struct rebind
  {
    typedef StackAllocator<T> other;
  };

  pointer address( reference x ) const
  {
      return 0;
  }

  const_pointer address( const_reference x ) const
  {
      return 0;
  }

  pointer allocate(size_type n)
  {
      head_ = head_ + n * sizeof(Type);
      return (pointer)head_;
  }

  void deallocate(pointer, size_type n)
  {
      head_ = head_ - n * sizeof(Type);
  }

  size_type max_size()
  {
      return 8 * 1024 * 1024;
  }

  template <class U, class... Args> void construct(U*, Args&&...) {}
};

#endif // STACK_ALLOCATOR_H


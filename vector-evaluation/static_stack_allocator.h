#ifndef STATIC_STACK_ALLOCATOR_H
#define STATIC_STACK_ALLOCATOR_H

#include <cstddef>

extern char ExtendedMemory[MAX_ALLOCATION_BUFFER_SIZE];

template<class Type>
class StaticStackAllocator
{
    char* head_;

public:
    typedef Type value_type;
    typedef Type* pointer;
    typedef Type& reference;
    typedef const Type* const_pointer;
    typedef const Type& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    StaticStackAllocator()
        : head_(ExtendedMemory)
    {
    }

    template <class T>
    struct rebind
    {
      typedef StaticStackAllocator<T> other;
    };

    pointer address( reference x ) const
    {
        return 0;
    }

    const_pointer address( const_reference x ) const
    {
        return 0;
    }

    pointer allocate(size_type n)
    {
        head_ = head_ + n * sizeof(Type);
        return (pointer)head_;
    }

    void deallocate(pointer, size_type n)
    {
        head_ = head_ - n * sizeof(Type);
    }

    size_type max_size()
    {
        return 8 * 1024 * 1024;
    }

    template <class U, class... Args> void construct(U*, Args&&...) {}
};


#endif // STATIC_STACK_ALLOCATOR_H


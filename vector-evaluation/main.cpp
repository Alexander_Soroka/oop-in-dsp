#include <iostream>
#include <vector>
#include <cstdlib>

#include "pool_allocator.h"
#include "malloc_allocator.h"
#include "new_allocator.h"
#include <stack_allocator.h>
#include "static_stack_allocator.h"
#include "scope_watch.h"

namespace
{
int kTestCount = 50000000;
int kTestArraySize = 1024;
}

template <template <class Type> class Allocator>
void PerformAllocationTest()
{
  ScopeWatch watch(__PRETTY_FUNCTION__);
  Allocator<double> allocator;
  for (int test_count = 0; test_count < kTestCount; ++test_count)
  {
    double* p1 = allocator.allocate(kTestArraySize);
    p1[0] = 0;
    allocator.deallocate(p1, kTestArraySize);
  }
}

template <>
void PerformAllocationTest<PoolAllocator>()
{
  ScopeWatch watch(__PRETTY_FUNCTION__);
  Initialize(10);

  PoolAllocator<double> allocator;
  for (int test_count = 0; test_count < kTestCount; ++test_count)
  {
    double* p1 = allocator.allocate(kTestArraySize);
    p1[0] = 0;
    allocator.deallocate(p1, kTestArraySize);
  }
}

template <template <class Type> class Allocator>
void PerformVectorAllocationTest()
{
  ScopeWatch watch(__PRETTY_FUNCTION__);
  for (int test_count = 0; test_count < kTestCount; ++test_count)
  {
    std::vector<double, Allocator<double> > v(kTestArraySize);
    v[0] = 1;
  }
}

template <>
void PerformVectorAllocationTest<PoolAllocator>()
{
  ScopeWatch watch(__PRETTY_FUNCTION__);
  Initialize(10);

  for (int test_count = 0; test_count < kTestCount; ++test_count)
  {
    std::vector<double, PoolAllocator<double> > v(kTestArraySize);
    v[0] = 1;
  }
}

int main()
{
  ScopeWatch watch(__FUNCTION__);

  InitializeStackAllocator(MAX_ALLOCATION_BUFFER_SIZE);

  PerformAllocationTest<NewAllocator>();
  PerformAllocationTest<MallocAllocator>();
  PerformAllocationTest<std::allocator>();

  PerformAllocationTest<PoolAllocator>();
  PerformAllocationTest<StackAllocator>();
  PerformAllocationTest<StaticStackAllocator>();

  PerformVectorAllocationTest<std::allocator>();
  PerformVectorAllocationTest<NewAllocator>();
  PerformVectorAllocationTest<MallocAllocator>();
  PerformVectorAllocationTest<PoolAllocator>();
  PerformVectorAllocationTest<StackAllocator>();
  PerformVectorAllocationTest<StaticStackAllocator>();

  return 0;
}

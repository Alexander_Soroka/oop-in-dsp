#include "scope_watch.h"

namespace
{
void diff(timespec* start, timespec* end, timespec* result);
}

ScopeWatch::ScopeWatch(const std::string& name) : name_(name)
{
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_);
}

ScopeWatch::~ScopeWatch()
{
  timespec stop_time = {0, 0};
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop_time);

  timespec elapsed = {0, 0};
  diff(&start_time_, &stop_time, &elapsed);

  double seconds = elapsed.tv_sec;
  double milliseconds = elapsed.tv_nsec / 1000000;
  double microseconds = elapsed.tv_nsec / 1000 - milliseconds * 1000;
  double nanoseconds = elapsed.tv_nsec - milliseconds * 1000000 - microseconds * 1000;

  std::cerr << "[" << name_ << "] : " <<
               seconds << " s, " <<
               milliseconds << " ms, " <<
               microseconds << " micros, " <<
               nanoseconds << " ns, " <<
               "raw [" << elapsed.tv_nsec << "]" << std::endl;
}


namespace
{
void diff(struct timespec *start, struct timespec *end, struct timespec * result)
{
  if ((end->tv_nsec - start->tv_nsec) < 0)
  {
    result->tv_sec = end->tv_sec - start->tv_sec - 1;
    result->tv_nsec = 1000000000 + end->tv_nsec-start->tv_nsec;
  }
  else
  {
    result->tv_sec = end->tv_sec - start->tv_sec;
    result->tv_nsec = end->tv_nsec - start->tv_nsec;
  }
}
}

#ifndef POOL_ALLOCATOR_H
#define POOL_ALLOCATOR_H

#include <cstddef>

struct Node
{
    double* data_;
    struct Node* next_;
};

extern Node* memory_pool_head;
extern Node* memory_pool_last;

void Initialize(size_t size);

template<class Type>
class PoolAllocator
{
    Node* current_;
public:
    typedef Type value_type;
    typedef Type* pointer;
    typedef Type& reference;
    typedef const Type* const_pointer;
    typedef const Type& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    PoolAllocator()
        : current_(memory_pool_head)
    {
    }

    template <class T>
    struct rebind
    {
      typedef PoolAllocator<T> other;
    };

    pointer address(reference x) const
    {
        return 0;
    }

    const_pointer address(const_reference x) const
    {
        return 0;
    }

    pointer allocate(size_type)
    {
        memory_pool_head = current_->next_;
        pointer memory = (pointer)current_->data_;

        memory_pool_last->next_ = current_;
        memory_pool_last = current_;
        memory_pool_last->data_ = nullptr;
        memory_pool_last->next_ = nullptr;

        return memory;
    }

    void deallocate(pointer value, size_type)
    {
        memory_pool_last->data_ = (double*)value;
    }

    size_type max_size()
    {
        return 8 * 1024 * 1024;
    }

    template <class U, class... Args> void construct(U*, Args&&...) {}
};

#endif // LINEAR_ALLOCATOR_H

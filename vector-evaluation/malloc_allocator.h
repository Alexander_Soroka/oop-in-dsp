#ifndef MALLOC_ALLOCATOR_H
#define MALLOC_ALLOCATOR_H

#include <cstddef>

template<class Type>
class MallocAllocator
{
public:
    typedef Type value_type;
    typedef Type* pointer;
    typedef Type& reference;
    typedef const Type* const_pointer;
    typedef const Type& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    template <class T>
    struct rebind
    {
      typedef MallocAllocator<T> other;
    };

    pointer address( reference x ) const
    {
        return 0;
    }

    const_pointer address( const_reference x ) const
    {
        return 0;
    }

    pointer allocate(size_type n)
    {
        return (pointer)malloc(n * sizeof(Type));
    }

    void deallocate(pointer p, size_type)
    {
        free(p);
    }

    size_type max_size()
    {
        return 8 * 1024 * 1024 * 1024;
    }

    template <class U, class... Args> void construct(U*, Args&&...) {}
};

#endif // MALLOC_ALLOCATOR_H


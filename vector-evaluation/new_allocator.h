#ifndef NEW_ALLOCATOR_H
#define NEW_ALLOCATOR_H

#include <cstddef>

template<class Type>
class NewAllocator
{
public:
    typedef Type value_type;
    typedef Type* pointer;
    typedef Type& reference;
    typedef const Type* const_pointer;
    typedef const Type& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    NewAllocator()
    {
    }

    template <class T>
    struct rebind
    {
      typedef NewAllocator<T> other;
    };

    pointer address(reference x) const
    {
        return 0;
    }

    const_pointer address(const_reference x) const
    {
        return 0;
    }

    pointer allocate(size_type n)
    {
        return new Type[n];
    }

    void deallocate(pointer value, size_type)
    {
        delete [] value;
    }

    size_type max_size()
    {
        return 8 * 1024 * 1024;
    }

    template <class U, class... Args> void construct(U*, Args&&...) {}
};

#endif // NEW_ALLOCATOR_H


#include <pool_allocator.h>

Node* memory_pool_head = nullptr;
Node* memory_pool_last = nullptr;

void Initialize(size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        Node* node = new Node;
        node->data_ = new double[1024];
        if (!memory_pool_head)
        {
            memory_pool_head = node;
        }
        if (!memory_pool_last)
        {
            memory_pool_last = node;
        }

        memory_pool_last->next_ = node;
        memory_pool_last = node;
    }
}


#ifndef SCOPE_WATCH_H
#define SCOPE_WATCH_H

#include <iostream>
#include <string>
#include <time.h>

class ScopeWatch
{
public:
  explicit ScopeWatch(const std::string& name);
  ~ScopeWatch();

private:
  std::string name_;
  timespec start_time_;
};


#endif // SCOPE_WATCH_H

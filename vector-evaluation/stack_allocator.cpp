#include "stack_allocator.h"

char* stack_memory_head = nullptr;

void InitializeStackAllocator(size_t size)
{
  stack_memory_head = new char[size];
}

#include <iostream>

class A
{
public:
  void operator=(int v)
  {
  }

};

template<class T>
class ourVector
{
  size_t size_;
  T *data_;

public:
  ourVector(size_t size)
  {
    size_ = size;
    data_ = new T [size_];
    for (int i=0; i<size_;i++)
    {
      data_[i] = 0;
    }
  }

  ~ourVector()
  {
    delete []data_;
  }

  T getValue(size_t index)
  {
    return data_[index];
  }

  void setValue(size_t index, T value)
  {
    data_[index] = value;
  }

  void resize(size_t size)
  {T *data_temp=new T [size];
    size_t min_size=std::min(size, size_);
    std::fill(data_temp,data_temp+size,0 );
    std::copy(data_, data_+min_size, data_temp);
    delete[] data_;
    data_=data_temp;
    size_=size;
  }

  T& operator[](size_t index)
  {
    return data_[index];
  }

};


int main(int argc, char* argv[])
{
  ourVector<A> a(10);
  
  for (int i = 0; i < 10; ++i)
  {
    //a[i] = "abc\n";
    //std::cout << a[i] << "  ";
  }

  return 0;
}

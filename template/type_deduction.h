#ifndef TYPE_DEDUCTION_H
#define TYPE_DEDUCTION_H

#include <iostream>

template <class Type>
class TypeDeduction
{
public:
    TypeDeduction()
    {
        Print(member_);
    }

    void Print(Type value)
    {
        std::cout << "This is generic implementation: " << value << std::endl;
    }

private:
    Type member_;
};

#endif // TYPE_DEDUCTION_H


#include <type_deduction.h>

template <>
void TypeDeduction<int>::Print(int value)
{
    std::cout << "This is int: " << value << std::endl;
}

template <>
void TypeDeduction<double>::Print(double value)
{
    std::cout << "This is double: " << value << std::endl;
}

template <>
void TypeDeduction<std::string>::Print(std::string value)
{
    std::cout << "This is string: " << value << std::endl;
}

#include <iostream>

#include <string>
#include <list>

std::ostream& operator<<(std::ostream& stream, const std::list<int>& list)
{
    for (const int& value : list)
    {
        stream << "\t" << value;
    }
    stream << std::endl;
}

#include <type_deduction.h>

int main()
{
    TypeDeduction<int> a;
    TypeDeduction<double> b;
    TypeDeduction<std::string> c;
    TypeDeduction<std::list<int> > d;

    return 0;
}


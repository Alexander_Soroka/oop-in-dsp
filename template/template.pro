TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    type_deduction.cpp

HEADERS += \
    type_deduction.h

#include "managed_complex.h"
#include "stdlib.h"

Next* ManagedComplex::allocation_list_ = 0;

ManagedComplex::ManagedComplex(double, double)
{
}

void* ManagedComplex::operator new(std::size_t)
{
    if (!allocation_list_)
    {
        expandTheFreeList();
    }

    Next *memory_for_object_ = allocation_list_;
    allocation_list_ = allocation_list_->next;
    return memory_for_object_;
}

void ManagedComplex::operator delete(void* p)
{
    Next *free = (Next*)p;
    free->next = allocation_list_;
    allocation_list_ = free;
}

void ManagedComplex::expandTheFreeList()
{
    allocation_list_ = (Next*)malloc(sizeof(ManagedComplex));
    allocation_list_->next = 0;
}

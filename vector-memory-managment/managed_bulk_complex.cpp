#include "managed_bulk_complex.h"
#include "stdlib.h"

List* ManagedBulkComplex::head_ = 0;

ManagedBulkComplex::ManagedBulkComplex(double, double)
{
}

void* ManagedBulkComplex::operator new(std::size_t)
{
    if (!head_)
    {
        const int kSize = 1024;
        head_ = (List*)malloc(sizeof(ManagedBulkComplex) * kSize);
        List* list_element = head_;
        for (int i = 0; i < kSize; ++i)
        {
            list_element->next = (List*)((char*)head_ + i * sizeof(ManagedBulkComplex));
            list_element = list_element->next;
        }
        list_element->next = 0;
    }

    List *new_object_ = head_;
    head_ = head_->next;
    return new_object_;
}

void ManagedBulkComplex::operator delete(void* p)
{
    List *free = (List*)p;
    free->next = head_;
    head_ = free;
}

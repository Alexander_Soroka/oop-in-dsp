#ifndef MANAGED_BULK_COMPLEX_H
#define MANAGED_BULK_COMPLEX_H

#include <iostream>

class List
{
public:
    List *next;
};

class ManagedBulkComplex
{
public:
    ManagedBulkComplex(double real, double imaj);

    void* operator new(std::size_t);
    void operator delete(void*);

private:
    double real_;
    double imaj_;

    static List* head_;
};

#endif // MANAGED_COMPLEX_H

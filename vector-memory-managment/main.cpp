#include <time.h>
#include <iostream>
#include <stdlib.h>
#include <vector>

#include <scope_watch.h>

#include <managed_complex.h>
#include "unmanaged_complex.h"
#include "managed_bulk_complex.h"

typedef void(*TestFunction)();

namespace
{
const int kSize = 1024;
}

void PerformTest(TestFunction f, const std::string& test_name)
{
    ScopeWatch scope_watch(test_name);
    for(int i = 0; i < 500; ++i)
    {
        for(int j = 0; j < 500; ++j)
        {
            f();
        }
    }
}

void EmptyTest() { }

void Malloc()
{
    double* array_pointer = (double*) malloc(sizeof(double*) * kSize);
    array_pointer[0]++;
    free(array_pointer);
}

void New()
{
    double* array_pointer = new double[kSize];
    array_pointer[0]++;
    delete [] array_pointer;
}

void Vector()
{
    std::vector<double> array(kSize);
    array[0]++;
}

void StaticVector()
{
    static std::vector<double> array;
    array.resize(kSize);
    array[0]++;
}

void ManagedComplexAllocation()
{
    ManagedComplex* complex_array[1000];
    for (int i = 0; i < 1000; ++i)
    {
        complex_array[i] = new ManagedComplex(i, 0);
    }

    for (int i = 0; i < 1000; ++i)
    {
        delete complex_array[i];
    }
}

void UnManagedComplexAllocation()
{
    UnManagedComplex* complex_array[1000];
    for (int i = 0; i < 1000; ++i)
    {
        complex_array[i] = new UnManagedComplex(i, 0);
    }

    for (int i = 0; i < 1000; ++i)
    {
        delete complex_array[i];
    }
}

void ManagedBulkComplexAllocation()
{
    ManagedBulkComplex* complex_array[1000];
    for (int i = 0; i < 1000; ++i)
    {
        complex_array[i] = new ManagedBulkComplex(i, 0);
    }

    for (int i = 0; i < 1000; ++i)
    {
        delete complex_array[i];
    }
}


int main()
{
    ScopeWatch scope_watch(__FUNCTION__);

    PerformTest(&EmptyTest, "Empty test");
    PerformTest(&Malloc, "Allocation via malloc");
    PerformTest(&New, "Allocation via new");
    PerformTest(&StaticVector, "Allocation via static vector");
    PerformTest(&ManagedComplexAllocation, "Managed class");
    PerformTest(&UnManagedComplexAllocation, "UnManaged class");
    PerformTest(&ManagedBulkComplexAllocation, "Bulk allocation");

    return 0;
}


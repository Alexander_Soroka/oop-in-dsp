TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle qt

LIBS += -lrt

SOURCES += main.cpp \
    ../vector-evaluation/scope_watch.cpp \
    unmanaged_complex.cpp \
    managed_complex.cpp \
    managed_bulk_complex.cpp

HEADERS += \
    ../vector-evaluation/scope_watch.h \
    unmanaged_complex.h \
    managed_complex.h \
    managed_bulk_complex.h

INCLUDEPATH += ../vector-evaluation


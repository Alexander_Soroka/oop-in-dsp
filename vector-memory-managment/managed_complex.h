#ifndef MANAGED_COMPLEX_H
#define MANAGED_COMPLEX_H

#include <iostream>

class Next
{
public:
    Next *next;
};

class ManagedComplex
{
public:
    ManagedComplex(double real, double imaj);

    void* operator new(std::size_t);
    void operator delete(void*);

    static void newMemPool() { expandTheFreeList(); }
    static void deleteMemPool();

private:
    double real_;
    double imaj_;
    static void expandTheFreeList();

    static Next* allocation_list_;
};

#endif // MANAGED_COMPLEX_H

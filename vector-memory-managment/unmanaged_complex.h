#ifndef UNMANAGED_COMPLEX_H
#define UNMANAGED_COMPLEX_H

// Complex class without memory managment

class UnManagedComplex
{
public:
    UnManagedComplex(double real, double imaj);

private:
    double real_;
    double imaj_;
};

#endif // UNMANAGED_COMPLEX_H
